package es.udc.fic.ri.p1;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.FieldInfo;
import org.apache.lucene.index.FieldInfos;
import org.apache.lucene.index.MultiTerms;
import org.apache.lucene.index.PostingsEnum;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;


public class SimilarTerms{
	
	public static void main(final String[] args) throws IOException {
		String usage = "java -jar SimilarTerms-0.0.1-SNAPSHOT-jar-with-dependencies.jar"+
				"[-index <index_folder>] [-field <field_folder>] [-term <term, field>]"+
				"[-top n] [-rep <bin | tf | tfxidf>]";
		String indexPath = "";
		String field = "";
		String[] termField = null;
		String term = "";
		int top;
		String rep = "";
		
		Directory dir = null;
		DirectoryReader reader = null;
		
		for(int i=0;i<args.length;i++) {
			if("-index".equals(args[i])) {
				indexPath = args[i+1];
				i++;
			}else if ("-field".equals(args[i])) {
				field = args[i+1];
				i++;
			}else if ("-term".equals(args[i])) {
				termField = args[i+1].split(",");
				term = termField[0];
			}else if ("-top".equals(args[i])) {
				top = Integer.parseInt(args[i+1]);
				i++;
			}else if ("-rep".equals(args[i])) {
				rep = args[i+1];
					i++;
			}
		}
			
		if(indexPath != null) {
			if("".equals(indexPath)) {
				System.out.println("No indices found in this path, usage: " + usage);
				System.exit(1);
			}
		} else {
			System.err.println("Path to index needed, usage: " + usage);
			System.exit(1);
		}
		
		if(!(rep.equals("bin") || rep.equals("tf") || rep.equals("tfxidf"))) {
			System.err.println("Unrecognized representation, usage: "+ usage);
			System.exit(1);
		}else if (rep.equals("")) {
			System.err.println("Representation mode needed, usage: " + usage);
			System.exit(1);
		}
		
		try {
			dir = FSDirectory.open(Paths.get(indexPath));
			reader = DirectoryReader.open(dir);
		} catch (CorruptIndexException e1) {
			System.out.println("yo q se meu " + e1);
			System.err.println("Corrupt index");
			e1.printStackTrace();
			System.exit(1);
		}
		
	}
	
}
